# Parameters that we can control much easier
import os
from tensorflow.keras import backend as KBackend

############################################
transfer = True
############################################

NUM_CLASSES = 2
LAYER_INDEX = 30
BATCH_SIZE = 32
EPOCHS = 50
TARGET_SIZE = (244,244)
train_directory = 'data/train/'
validation_directory = 'data/validation/'
test_directory = 'data/test/'
list_of_images = os.listdir(test_directory)
if KBackend.image_data_format() == 'chanels_first':
    INPUT_SHAPE = (3, 244, 244)
else:
    INPUT_SHAPE = (244, 244, 3)
normal_weight_path = 'saved_weights/normal.h5'
fine_tuned_weight_path = 'saved_weights/fine_tuned.h5'

classes = ['hotdog', 'not hotdog']
