# Docker Build instructions
You will need to setup a ngrok account which will generate a token which will link it to your account. Refer to ```Dockerfile``` on where to insert the line of code.

## Some notes on ```aux.py```
If you refer to the code in aux.py - you can see that there was some code that is largely commented. This was the code that was appropriate to run tensorflow lite.
