import pyqrcode
from pyqrcode import QRCode
import requests

r = requests.get('http://localhost:4040/api/tunnels')
tunnel_dict = r.json()
tunnel_dict = tunnel_dict['tunnels'][0]

# String which represent the QR code
s = tunnel_dict['public_url']

# Generate QR code
url = pyqrcode.create(s)

url.png('code.png', scale=8)

# import ezgmail
# import datetime
#
# ezgmail.send('summerstudio2020@gmail.com', datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y"),
#     s,['code.png'])
