# from tflite_runtime.interpreter import Interpreter
# from PIL import Image
import numpy as np
import os
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.applications.inception_v3 import preprocess_input
from tensorflow.keras.preprocessing import image

# def process_image(path):
#     image = Image.open(path).convert('RGB').resize((299,299),
#         Image.ANTIALIAS)
#     x = np.array(image, dtype=np.float32)
#     x = np.expand_dims(x, axis=0)
#     x =  x / 127.5
#     x = x - 1
#     return x

def process_image(path):
    # Preprocess exactly how it is done as per documentation
    img = image.load_img(path, target_size=(299,299))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    return x

def get_class(output):
    if output >= 0.5:
        return 'hotdog'
    else:
        return 'not hotdog'

class Model():
    def __init__(self):
        # self.interpreter = Interpreter(model_path='hotdog.tflite')
        # self.interpreter.allocate_tensors()
        # self.input_details = self.interpreter.get_input_details()
        # self.output_details = self.interpreter.get_output_details()
        self.interpreter = load_model('hotdog.h5')

    def get_output(self, x):
        # This function takes the input details and output input_details
        # that you run below, the model itself and the input data. it will
        # return you a tensor value
        # self.interpreter.set_tensor(self.input_details[0]['index'], x)
        # self.interpreter.invoke()
        # results = self.interpreter.get_tensor(self.output_details[0]['index'])
        results = self.interpreter.predict(x)
        return results
