from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation, Dropout
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras import backend as KBackend
import matplotlib.pyplot as plt
import numpy as np
import cv2
import os

# Directories for where the datasets are stored
train_directory = 'train/'
test_directory = 'test/'

# Reading an image to grab the input shape
cactus_images = os.listdir(train_directory + 'cactus/')
img = cv2.imread(train_directory + 'cactus/' + cactus_images[0], 0)
img_shape = img.shape

# Data Generators
datagen = ImageDataGenerator()

# Image Resize Factor
# Note that because my computer only has 4GB RAM - I had to scale the images
# down as I ran out of memory. If you got more memory - feel free to decrease
# the resize factor.

resize_factor = 5

# Script to ensure that the image shape is padded into the input correctly
# Copy and paste this script to make sure
if KBackend.image_data_format() == 'channels_first':
    INPUT_SHAPE = (3,int(img.shape[0] / resize_factor),
        int(img.shape[1] / resize_factor))
else:
    INPUT_SHAPE = (int(img.shape[0] / resize_factor),
        int(img.shape[1] / resize_factor),3)

# This function generates the train generator
train_generator = datagen.flow_from_directory(train_directory,
    target_size=(INPUT_SHAPE[0],INPUT_SHAPE[1]),
    batch_size=16, class_mode='categorical')

# Specifying the model details. Feel free to edit as appropriate
model = Sequential()
model.add(Conv2D(64, kernel_size=3, activation='relu', input_shape=INPUT_SHAPE))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(10, activation='softmax'))
model.summary()

model.compile(optimizer='adam', loss='categorical_crossentropy',
    metrics=['accuracy'])

# Hyperparameters
epochs = 3

# Fit_generator is now deprecated. Just use fit - we don't need to specify
# steps as it is automated now.
training_history = model.fit(train_generator, epochs=epochs, verbose=1)

# See what available data we have from the training_history object
print(training_history.history.keys())

# Plot the accuracy over training
plt.plot(training_history.history['accuracy'])
plt.title('Model Accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.show()

# Plot the loss over training
plt.plot(training_history.history['loss'])
plt.title('Model Loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.show()
