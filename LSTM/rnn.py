import numpy as np
from tensorflow.keras.layers import Dense, Activation
from tensorflow.keras.layers import SimpleRNN
from tensorflow.keras.models import Sequential

# Clean the data of line breaks and non-ascii characters
baconFile = open('lyrics.txt', 'rb')
baconLines = []
for line in baconFile:
	line = line.strip().lower()
	line = line.decode("ascii", "ignore")
	if len(line) == 0:
		continue
	baconLines.append(line)
baconFile.close()
baconText = " ".join(baconLines)

# Convert the characters into indexes and vice-versa to construct a look-up table
chars = set([c for c in baconText])
nb_chars = len(chars)
char2index = dict((c, i) for i, c in enumerate(chars))
index2char = dict((i, c) for i, c in enumerate(chars))

# Construct training and test labels 
# Training input data is considered a sequence of characters
# Testing data is considered the next character after that sequence
SEQUENCE_LENGTH = 30
STEP = 1

input_chars = []
label_chars = []
for num in range(0, len(baconText) - SEQUENCE_LENGTH, STEP):
	input_chars.append(baconText[num:num + SEQUENCE_LENGTH])
	label_chars.append(baconText[num + SEQUENCE_LENGTH])

for num in range(0, 100, 20):
	print('Sample train: ' + str(input_chars[num]) + '\nSample test:' + str(label_chars[num]))

# One-hot encode our data by checking our look-up table 
X = np.zeros((len(input_chars), SEQUENCE_LENGTH, nb_chars), dtype=np.bool)
y = np.zeros((len(input_chars), nb_chars), dtype=np.bool)
for i, input_char in enumerate(input_chars):
	for j, ch in enumerate(input_char):
		X[i, j, char2index[ch]] = 1
	y[i, char2index[label_chars[i]]] = 1

# Compile our simple model
HIDDEN_SIZE = 300
BATCH_SIZE = 300
NUM_ITERATIONS = 5
NUM_EPOCHS_PER_ITERATION = 3
NUM_PREDS_PER_EPOCH = 100

model = Sequential()
model.add(SimpleRNN(HIDDEN_SIZE, return_sequences=False, input_shape=(SEQUENCE_LENGTH, nb_chars), unroll=True))
model.add(Dense(nb_chars))
model.add(Activation('softmax'))
model.compile(loss = 'categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

# Training
# For each iteration, we want to see a random excerpt. 

for iteration in range(NUM_ITERATIONS):
	print("=" * 50)
	print("Iteration #: %d" %(iteration))
	model.fit(X, y, batch_size=BATCH_SIZE, 
		verbose = 2, epochs=NUM_EPOCHS_PER_ITERATION)

	test_idx = np.random.randint(len(input_chars))
	test_chars = input_chars[test_idx]
	print("Generating from seed: %s" %(test_chars))
	print(test_chars, end="")

	for i in range(NUM_PREDS_PER_EPOCH):
		Xtest = np.zeros((1, SEQUENCE_LENGTH, nb_chars))
		for i, ch in enumerate(test_chars):
			Xtest[0, i, char2index[ch]] = 1
		pred = model.predict(Xtest, verbose=0)[0]
		ypred = index2char[np.argmax(pred)]
		print(ypred, end="")
		test_chars = test_chars[1:] + ypred
print()
