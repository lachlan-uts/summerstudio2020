import numpy as np
import matplotlib.pyplot as plt 
import os
import tensorflow as tf
from tensorflow.keras import backend as KBackend
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.xception import preprocess_input
from tensorflow.keras.models import load_model

# all possible classifications
classes = ['hotdog', 'nothotdog']

model = tf.lite.Interpreter(model_path="tf_lite_model.tflite")
model.allocate_tensors()


def model_predict(image_path, model):
	
	#loading the image
	input_details = model.get_input_details()
	output_details = model.get_output_details()

	img = image.load_img(image_path, target_size=(224,224))
	
	#preprocessing the image
	img_array = image.img_to_array(img)
	img_array_expand_dims = np.expand_dims(img_array, axis=0)
	processed_img = preprocess_input(img_array_expand_dims)

	model.set_tensor(input_details[0]['index'], processed_img)
	model.invoke()
	output = model.get_tensor(output_details[0]['index'])

	prediction = np.squeeze(output)

	prediction_index = prediction.argmax()
	prediction = np.max(prediction)

	# must return output in string format to be used by fuctions in main.py
	resultFinal = str(classes[prediction_index])
	return resultFinal

